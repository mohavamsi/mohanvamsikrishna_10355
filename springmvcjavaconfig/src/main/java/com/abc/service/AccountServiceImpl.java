package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDAO;
import com.abc.hibernate.entities.Account;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountDAO accountDAO;
		
	/**
	 * This method is used to display the account by number
	 */
	@Transactional
	@Override
	public Account searchAccountByID(int accno) {
		// TODO Auto-generated method stub
		return accountDAO.getAccountByID(accno);
	}
	/**
	 * This method is used to insert the account into the table 
	 */
	@Transactional
	@Override
	public boolean createAccount(Account account) {
		// TODO Auto-generated method stub
		return accountDAO.createAccount(account);
	}
	/**
	 * This method returns the account is deleted from table or not
	 */
	@Transactional
	@Override
	public boolean deleteAccount(Account account) {
		return accountDAO.deleteAccount(account);
	}
	/**
	 * This method is used to update the account details.
	 */
	@Transactional
	@Override
	public Account updateAccount(Account account) {
		// TODO Auto-generated method stub
		return accountDAO.updateAccount(account);
	}

}
