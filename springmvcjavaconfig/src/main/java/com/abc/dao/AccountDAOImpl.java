package com.abc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.hibernate.entities.Account;

@Repository
public class AccountDAOImpl implements AccountDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * This method is used to display the account by number
	 */
	@Override
	public Account getAccountByID(int accno) {
		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, accno);
		return account;
	}
	
	/**
	 * This method is used to insert the account by number
	 */
	@Override
	public boolean createAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		session.save(account);
		return true ;
	}
	
	/**
	 * This method is used to delete the account by number
	 */

	@Override
	public boolean deleteAccount(Account account) {
		Session session =  sessionFactory.getCurrentSession();
		session.delete(account);
		return true;
	}
	/**
	 * This method is used to update the account in database.
	 */
	@Override
	public Account updateAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		session.update(account);
		return account;
	}
	

	
}
