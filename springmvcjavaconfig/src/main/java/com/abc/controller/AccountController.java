package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;

	@PostMapping("/account/create")
	public ResponseEntity<?> createNewAccount(@RequestBody Account account) {
		boolean id = accountService.createAccount(account);

		return new ResponseEntity("New account is created with id:" + id, HttpStatus.OK);
	}

//	@GetMapping("/account/{id}")
//	public ResponseEntity<Account> get(@PathVariable("id") int id) {
//		Account account = accountService.searchAccountByID(id);
//		if (account == null) {
//
//			return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
//		}
//		return new ResponseEntity<Account>(account, HttpStatus.OK);
//	}
//
//	@GetMapping("/account")
//	public ResponseEntity<List<Account>> list() {
//		List<Account> accounts = accountService.getAllAccountList();
//		if (accounts.isEmpty()) {
//			return new ResponseEntity<List<Account>>(HttpStatus.NO_CONTENT);
//		}
//		return new ResponseEntity<List<Account>>(accounts, HttpStatus.OK);
//	}
//
//	@PutMapping("/account/{id}")
//	public ResponseEntity<?> update(@PathVariable("id") int accno, @RequestBody Account account) {
//		Account currentAccount = accountService.updateAccount(account);
//
//		if (currentAccount == null) {
//			return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
//		}
//
//		currentAccount.setAccno(account.getAccno());
//		currentAccount.setName(account.getName());
//		currentAccount.setBalance(account.getBalance());
//		accountService.updateAccount(currentAccount);
//		return new ResponseEntity<Account>(currentAccount, HttpStatus.OK);
//	}
//
//	@DeleteMapping("/account/{id}")
//	public ResponseEntity<?> delete(@PathVariable("id") int accno) {
//
//		Account account = accountService.deleteAccount(account);
//		if (account == null) {
//
//			return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
//		}
//		accountService.delete(accno);
//		return new ResponseEntity<Account>(HttpStatus.NO_CONTENT);
//
//	}
}
