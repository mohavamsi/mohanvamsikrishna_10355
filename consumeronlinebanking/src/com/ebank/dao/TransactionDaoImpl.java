package com.ebank.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ebank.beans.UserTransactionBean;
import com.ebank.utility.DButil;

/**
 * This class deals with data base tables like fetching records and updating
 * them. This class mainly deals with transaction related tables.
 * 
 * @author Team-E
 *
 */
public class TransactionDaoImpl implements TransactionDao {

	/**
	 * This is an implementation of TransactionDao fetchBalance(int) method. This
	 * method fetches balance details from table based on account number.
	 * 
	 */
	@Override
	public double fetchBalance(int accno) { 
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		double balance = 0.0d;
		// preparing sql query for balance enquiry
		String sql = "select balance from AccountDetails where accountNum = ?";
		try {
			// getting connection object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, accno);
			// executing sql query
			rs = ps.executeQuery();
			if (rs.next())
				balance = rs.getDouble(1);
		} catch (Exception e) {

		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return balance;
	} // End of fetchBalance

	/**
	 * This is an implementation of TransactionDao updateBalance(int, doubel)
	 * method. This method updates balance field of specific row based on account
	 * number.
	 */
	@Override
	public boolean updateBalance(int accno, double balance) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean flag = false;
		// preparing sql query for
		String sql = "update AccountDetails set balance = ? where accountNum = ?";
		try {
			// getting connection object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setDouble(1, balance);
			ps.setInt(2, accno);
			// executing sql query
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected > 0)
				flag = true;
		} catch (Exception e) {

		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	} // End of updateBalance

	/**
	 * This method checks whether account exist or not
	 */
	@Override
	public boolean isAccountExist(int accno) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean isExist = false;
		// preparing sql query for accountNumber retrieval from accountdetails table
		String sql = "select accountNum from AccountDetails where accountNum = ?";
		try {
			// getting connection object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, accno);
			// executing sql query
			rs = ps.executeQuery();
			if (rs.next())
				isExist = (rs.getInt(1) == accno) ? true : false;
		} catch (Exception e) {

		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return isExist;
	} // End of isAccountExist

	/**
	 * This is implementation of TransactionDao
	 * insertTransacation(UserTrnsactionBean) method
	 */
	@Override
	public boolean insertTransaction(UserTransactionBean txBean) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean flag = false;
		// preparing sql query for
		String sql = "insert into transactiondetails (accountNumber,dateoftransaction,message,debitamount,creditamount,status) values (?,?,?,?,?,?)";
		try {
			// getting connection object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, txBean.getAccountNumber());
			ps.setDate(2, new Date(txBean.getTransactionDate().getTime()));
			ps.setString(3, txBean.getMessage());
			ps.setDouble(4, txBean.getDebitAmount());
			ps.setDouble(5, txBean.getCreditAmount());
			ps.setString(6, txBean.getStatus());
			
			// executing sql query
			int rowsEffected = ps.executeUpdate();
			System.out.println(rowsEffected);
			if (rowsEffected == 1) {
				flag = true;
			} else {
				System.out.println("Record is not inserted");
			}
		} catch (Exception e) {
e.printStackTrace();
		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
		
	} // End of insertTransaction

	/**
	 * This method fetches transaction details from database tables and returns list
	 * of transactions
	 */
	@Override
	public List<UserTransactionBean> fetchTransactions(int accNum) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<UserTransactionBean> txList = new ArrayList<>();
		// preparing sql query for
		String sql = "select transactionId,accountNumber,dateoftransaction,message,debitamount,creditamount,status from transactiondetails where accountNum = ?";
		try {
			// getting connection object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, accNum);

			// executing sql query
			rs = ps.executeQuery();
			while (rs.next()) {
				// creating UserTransactionBean object
				UserTransactionBean txBean = new UserTransactionBean();
				// setting values to the bean object from resultset
				txBean.setTransactionId(rs.getLong(1));
				txBean.setAccountNumber(rs.getInt(2));
				txBean.setTransactionDate(rs.getDate(3));
				txBean.setMessage(rs.getString(4));
				txBean.setDebitAmount(rs.getDouble(5));
				txBean.setCreditAmount(rs.getDouble(6));
				txBean.setStatus(rs.getString(7));
				// adding bean object to the list
				txList.add(txBean);
			} // End of while
		} catch (Exception e) {

		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return txList;
	} // End of fetchTransactions
}