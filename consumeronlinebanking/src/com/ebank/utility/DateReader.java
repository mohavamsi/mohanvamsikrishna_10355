package com.ebank.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateReader {

	/**
	 * This method checks whether date is valid or not
	 * 
	 * @param d
	 * @param m
	 * @param y
	 * @return true if valid else false
	 */
	public boolean isValidDate(int d, int m, int y) {
		int MAX_VALID_YR = 9999;
		int MIN_VALID_YR = 1800;
		if (y > MAX_VALID_YR || y < MIN_VALID_YR)
			return false;
		if (m < 1 || m > 12)
			return false;
		if (d < 1 || d > 31)
			return false;

		if (m == 2) {
			if (isLeap(y))
				return (d <= 29);
			else
				return (d <= 28);
		}
		if (m == 4 || m == 6 || m == 9 || m == 11)
			return (d <= 30);

		return true;
	} // End of isValidDate

	/**
	 * This method checks Leap year or not
	 * 
	 * @param year
	 * @return true if leap else false
	 */
	public boolean isLeap(int year) {
		if (year % 400 == 0)
			return true;

		if (year % 100 == 0)
			return false;

		if (year % 4 == 0)
			return true;
		return false;
	} // End of checkYear

	/**
	 * This method asks user to enter date of birth and returns date
	 * 
	 * @return
	 */
	public Date readDate() {
		int year = 0;
		int month = 0;
		int date = 0;

		UserInputReader reader = new UserInputReaderImpl();
		boolean flag = false;
		// validating year
		while (!flag) {
			try {
				System.out.println("please enter year in yyyy format. ex:1998 ");
				int inputYear = reader.readInt();
				int calendarYear = Calendar.getInstance().get(Calendar.YEAR);
				calendarYear = calendarYear - 18;
				flag = (1947 < inputYear && inputYear <= calendarYear) ? true : false;
				if (!flag)
					System.out.println("year upto " + year + " only elegible.");
				year = inputYear;
			} catch (Exception e) {
				System.out.println("please enter a valid input");
			}
		}
		// validating month
		flag = false;
		while (!flag) {
			try {
				System.out.println("please enter month in mm format. ex:03 ");
				int inputMonth = reader.readInt();
				flag = (0 < inputMonth && inputMonth <= 12) ? true : false;
				if (!flag)
					System.out.println("please enter month between 1 and 12 only");
				month = inputMonth;
			} catch (Exception e) {
				System.out.println("please enter a valid input");
			}

		}

		flag = false;
		while (!flag) {
			try {
				System.out.println("please enter date in dd format. ex:05 ");
				int inputDate = reader.readInt();
				flag = this.isValidDate(inputDate, month, year);
				date = inputDate;
			} catch (Exception e) {
				System.out.println("please enter a valid input");
			}

		}
		Date dateOfBirth = null;
		try {
			dateOfBirth = new SimpleDateFormat("dd/MM/yyyy").parse(date + "/" + month + "/" + year);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateOfBirth;

	} // askUserDOB
}
