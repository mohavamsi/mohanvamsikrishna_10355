package com.ebank.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.ebank.beans.UserTransactionBean;
import com.ebank.dao.TransactionDaoImpl;

/**
 * This class contains functionalities which deals with user transactions. And
 * this class provides implementation of TransactionServices interface methods.
 * 
 * @author Team-E
 *
 */
public class TransactionServiceImpl implements TransactionServices {

	/**
	 * This method retrieves user balance information.
	 */
	@Override
	public double doBalEnq(int accNum) {
		return new TransactionDaoImpl().fetchBalance(accNum);
	} // End of doBalEnq

	/**
	 * This method contains logic to perform deposit operation
	 */
	@Override
	public boolean doDeposit(double amount, int accNum, UserTransactionBean txBean) {
		boolean isDeposited = false;
		// fetching current balance
		double balance = doBalEnq(accNum);
		// preparing UserTransactionBean object
		txBean.setAccountNumber(accNum);
		txBean.setCreditAmount(amount);
		txBean.setTransactionDate(Calendar.getInstance().getTime());
		// adding depositing amount to current balance and updating balance
		isDeposited = new TransactionDaoImpl().updateBalance(accNum, (balance + amount));

		txBean.setStatus(isDeposited ? "success" : "failed");
		// storing transaction details by calling dao method
		new TransactionDaoImpl().insertTransaction(txBean);
		return isDeposited;
	} // End of doDeposit

	/**
	 * This method contains logic to perform withdraw operation
	 */
	@Override
	public boolean doWithdraw(double amount, int accNum, UserTransactionBean txBean) {
		// TODO exception throw implementation
		boolean isWithdrawn = false;
		// fetching current balance
		double balance = doBalEnq(accNum);
		// preparing UserTransactionBean object
		txBean.setAccountNumber(accNum);
		txBean.setDebitAmount(amount);
		txBean.setTransactionDate(Calendar.getInstance().getTime());

		// subtracting withdrawal amount from current balance and updating balance if
		// amount is valid
		if (balance >= amount)
			isWithdrawn = new TransactionDaoImpl().updateBalance(accNum, (balance - amount));
		txBean.setStatus(isWithdrawn ? "success" : "failed");
		// storing transaction details by calling dao method
		new TransactionDaoImpl().insertTransaction(txBean);
		// returning balance
		return isWithdrawn;
	} // End of doWithdraw

	/**
	 * This method performs amount transfer between two accounts
	 */
	@Override
	public boolean doTransferAmount(int fromAccountNum, int toAccountNum, double amount, UserTransactionBean txBean) {
		boolean isTransferred = false;
		// if toAccountNum exist then transfer operation will perform
		if (new TransactionDaoImpl().isAccountExist(toAccountNum)) {
			// deducting amount from sender account
			boolean isDeducted = doWithdraw(amount, fromAccountNum, txBean);
			// crediting amount to receiver account
			if (isDeducted)
				isTransferred = doDeposit(amount, toAccountNum, txBean);
		}
		return isTransferred;
	} // End of doTransferAmount

	@Override
	public double doBillPayment(double Amount, int AccountNumber) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * This method fetches last five transactions of specific account.
	 */
	@Override
	public List<UserTransactionBean> miniStatement(int accountNumber) {
		List<UserTransactionBean> txList = transactionHistory(accountNumber);
		List<UserTransactionBean> miniList = new ArrayList<>();
		int count =0;
		for(int i=txList.size()-1;i>=0;i--) {
			if(count == 5)
				break;
			miniList.add(txList.get(i));
			count++;
		}
		return miniList;
	} // End of miniStatement

	/**
	 * This method used to retrieve transactions done through specific accountNumber
	 */
	@Override
	public List<UserTransactionBean> transactionHistory(int accountNumber) {

		return new TransactionDaoImpl().fetchTransactions(accountNumber);
	} // End of transactionHistory

}
