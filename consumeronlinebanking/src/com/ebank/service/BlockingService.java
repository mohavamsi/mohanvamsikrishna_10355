package com.ebank.service;

public interface BlockingService {
public boolean isBlocked(int accountNumber);
public boolean blockOrUnblockUser(int accountNumber);
}
