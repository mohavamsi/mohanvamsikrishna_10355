package com.ebank.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ebank.beans.AdminBean;
import com.ebank.service.*;
public class AdminLogin {

/**
 * Servlet implementation class AdminLoginServlet
 */
	@WebServlet("/AdminLogin")
	public class AdminLoginServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		public AdminLoginServlet() {
			super();
			// TODO Auto-generated constructor stub
		}

	    /**
		 * This method is used to send response to client in the form html page by
		 * taking the request from the client in html format
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			int adminId = Integer.parseInt(request.getParameter("id"));
			String password = request.getParameter("pwd");
			response.setContentType("text/html");
			AdminBean admin = new AdminBean();
			AdminService adminservice = new AdminServiceImpl();
			admin = adminservice. getAdminById(adminId);
			RequestDispatcher rd=request.getRequestDispatcher("Login.jsp");
			//this checks the admin and password. if both matches then the admin will be logged in
			if (adminId==admin.getAdminId() && password.equals(admin.getPassword())) {
				request.setAttribute("admin", admin);
				rd.forward(request, response);
			} else {
				response.sendRedirect("errorlogin.html");
			}
		}

	}

	
}
