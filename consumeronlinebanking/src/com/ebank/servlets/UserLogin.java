package com.ebank.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ebank.beans.UserBean;
import com.ebank.service.*;
public class UserLogin {

/**
 * Servlet implementation class UserLoginServlet
 */
	@WebServlet("/UserLoginServlet")
	public class UserLoginServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		public UserLoginServlet() {
			super();
			// TODO Auto-generated constructor stub
		}

	    /**
		 * This method is used to send response to client in the form html page by
		 * taking the request from the client in html format
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			int userId = Integer.parseInt(request.getParameter("id"));
			String password = request.getParameter("pwd");
			response.setContentType("text/html");
			UserBean user = new UserBean();
			UserService userservice = new UserServiceImpl();
			user = userservice.getUserById(userId);
			RequestDispatcher rd=request.getRequestDispatcher("UserLogin.jsp");
			//this checks the user and password. if both matches then the user will be logged in
			if (userId==user.getUserId() && password.equals(user.getPassword())) {
				request.setAttribute("user", user);
				rd.forward(request, response);
			} else {
				response.sendRedirect("errorlogin.html");
			}
		}

	}

	
}
