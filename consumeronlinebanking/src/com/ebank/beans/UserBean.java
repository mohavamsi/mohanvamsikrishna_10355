package com.ebank.beans;

import java.util.Date;

public class UserBean {

	
	private int userId;
	private String firstName;
	private String lastName;
	private String password;
	private String phoneNum;
	private String mailId;
	private Date dateOfBirth;
	private String gender;
	private AccountBean account;
	private UserTransactionBean txBean;

	public AccountBean getAccount() {
		return account;
	}

	public void setAccount(AccountBean account) {
		this.account = account;
	}

	public UserTransactionBean getTxBean() {
		return txBean;
	}

	public void setTxBean(UserTransactionBean txBean) {
		this.txBean = txBean;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "UserBean [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", password="
				+ password + ", phoneNum=" + phoneNum + ", mailId=" + mailId + ", dateOfBirth=" + dateOfBirth
				+ ", gender=" + gender + ", account=" + account + ", txBean=" + txBean + "]";
	}

	
}
