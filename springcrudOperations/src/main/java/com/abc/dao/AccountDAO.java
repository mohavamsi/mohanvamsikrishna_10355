package com.abc.dao;

import com.abc.hibernate.entities.Account;

public interface AccountDAO {
	//get Account details by account number
	Account getAccountByID(int accno);
	//creating a new account in database 
	boolean createAccount(Account account);
	//delete an account by account number
	boolean deleteAccount(Account account);
	//update a account by passing account object 
	Account updateAccount(Account account);
}
