package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

@Controller
public class AccountController {

	@Autowired
	private AccountService accountService;
	
	@RequestMapping("/accounts/{id}")
	public String searchAccount(@PathVariable("id") int accno,ModelMap map) {
		Account account = accountService.searchAccountByID(accno);
		map.addAttribute("account", account);
		return "Accountdetails";
	}
	
	@GetMapping("/createform")
	public String callInsertionform() {
		return "insertion";
	}
	@PostMapping("/create")
	public String insertAccount(@ModelAttribute Account account, ModelMap map) {
		boolean flag = accountService.createAccount(account);
		return flag ? "insert_success" : "insert_failure";
	}
	@GetMapping("/deleteform")
	public String callDeletionform() {
		return "deletion";
	}
	@RequestMapping("/delete/{id}")
	public String deleteAccount(@PathVariable("id") int accno)  {
		Account account = new Account();
		account.setAccno(accno);
		boolean flag = accountService.deleteAccount(account);
		return flag ? "deletion_success" : "deletion_failure"; 
	}
	@PostMapping("accounts/update")
	public String updateAccount(@ModelAttribute Account account, ModelMap map) {
		map.addAttribute("account", accountService.updateAccount(account));
		map.addAttribute("message","success fully updated");
		return "accountDetails";
	}
}






