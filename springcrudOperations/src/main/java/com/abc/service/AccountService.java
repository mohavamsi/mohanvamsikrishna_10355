package com.abc.service;

import com.abc.hibernate.entities.Account;

public interface AccountService {
	//display the account by account number
	Account searchAccountByID(int accno);
	//insert a new record into the table
	boolean createAccount(Account account);
	//this method delete account from table
	boolean deleteAccount(Account account);
	//this method is used to update account details
	Account updateAccount(Account account);
}
