package com.ebank.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ebank.beans.AccountBean;
import com.ebank.beans.UserBean;
import com.ebank.service.UserService;
import com.ebank.service.UserServiceImpl;

/**
 * This class contains test cases for UserService class functionalities.
 * 
 * @author IMVIZAG
 *
 */
public class UserServiceTestImpl implements UserServiceTest {

	UserService user = null;

	/**
	 * This method used to initialize values before test case execution.
	 */
	@Before
	public void setUp() {
		user = new UserServiceImpl();
	}

	/**
	 * This method used to close the resource that open in setup after test case
	 * execution.
	 */
	@After
	public void tearDown() {
		user = null;
	}

	/**
	 * This method used to test userService isUser method for positive case
	 */
	@Test
	public void isUserTestPositive() {
		boolean result = user.isUser(10345);
		assertTrue(result);
	}

	/**
	 * This test case method used to test userService isUser method for negative
	 * case
	 */
	@Test
	public void isUserTestNegative() {
		boolean result = user.isUser(100);
		assertFalse(result);
	}

	/**
	 * This test case method used to test userService doLogin method for positive
	 * case
	 */
	@Test
	@Override
	public void doLoginTestPositive() {
		boolean result = user.doLogin(10345, "Navy@123");
		assertTrue(result);
	}

	/**
	 * This test case method used to test userService doLogin method for negative
	 * case
	 */
	@Test
	@Override
	public void doLoginTestNegative() {
		boolean result = user.doLogin(10345, "Navy@12");
		assertFalse(result);
	}

	/**
	 * This test case is used to test userService doRegistration method for positive
	 * case which is used to Register a user.
	 */
	@Test
	@Override
	public void doRegistrationTestPositive() {
		// creating UserBean object and setting values
		UserBean userbean = new UserBean();
		userbean.setUserId(10348);
		userbean.setFirstName("haritha");
		userbean.setLastName("hari");
		userbean.setPassword("Hari@123");
		userbean.setPhoneNum("9959286390");
		userbean.setMailId("hari@gmail.com");
		try {
			userbean.setDateOfBirth(new SimpleDateFormat("dd/mm/yyyy").parse("10/10/1996"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		userbean.setGender("f");
		// creating AccountBean object and setting values
		AccountBean accountbean = new AccountBean();
		accountbean.setAccountHolderName("haritha");
		accountbean.setBalance(50000);
		accountbean.setNomineeName("venki");
		accountbean.setTypeOfAccount("savings");
		accountbean.setOpeningDate(Calendar.getInstance().getTime());
		accountbean.setStatus("active");
		accountbean.setUserId(userbean.getUserId());
		userbean.setAccount(accountbean);
		boolean result = user.doRegistration(userbean, accountbean);
		assertTrue(result);
	}

	/**
	 * This test case is used to test userService doRegistration method for negative
	 * case which is used to Register a user.
	 */
	@Test
	@Override
	public void doRegistrationTestNegative() {
		UserBean userbean = new UserBean();
		AccountBean accountbean = new AccountBean();
		boolean result = user.doRegistration(userbean, accountbean);
		assertFalse(result);
	}

	/**
	 * This test case is used to test userService fetchAllUsers method for positive
	 * case.
	 */
	@Test
	@Override
	public void fetchAllUsersTestPositive() {
		List<UserBean> list = user.fetchAllUsers();
		assertNotNull(list);
	}

	/**
	 * This test case is used to test userService fetchAllUsers method to test
	 * whether it is empty or not.
	 * 
	 */
	@Test
	@Override
	public void fetchAllUsersTestNegative() {
		List<UserBean> list = user.fetchAllUsers();
		assertFalse(list.isEmpty());
	}

	/**
	 * This test case is used to test userService getUserById method for positive
	 * case.
	 */
	@Test
	@Override
	public void getUserByIdTestPositive() {
		UserBean userbean = user.getUserById(10345);
		assertNotNull(userbean);
	}

	/**
	 * This test case is used to test userService getUserById method for negative
	 * case.
	 */
	@Test
	@Override
	public void getUserByIdTestNegative() {
		UserBean userbean = user.getUserById(10300);
		assertNull(userbean);
	}

	/**
	 * This test case is used to test userService updatePassword method for positive
	 * case.
	 */
	@Test
	@Override
	public void updatePasswordTestPositive() {
		boolean result = user.updatePassword(10359, "Mahesh@03");
		assertTrue(result);
	}

	/**
	 * This test case is used to test userService updatePassword method for Negative
	 * case.
	 */
	@Test
	@Override
	public void updatePasswordTestNegative() {
		boolean result = user.updatePassword(10300, "Mahesh@03");
		assertFalse(result);
	}

	/**
	 * This test case is used to test userService updateFirstName method for
	 * positive case.
	 */
	@Test
	@Override
	public void updateFirstNameTestPositive() {
		boolean result = user.updateFirstName(10359, "mahesh");
		assertTrue(result);
	}

	/**
	 * This test case is used to test userService updateFirstName method for
	 * negative case.
	 */
	@Test
	@Override
	public void updateFirstNameTestNegative() {
		boolean result = user.updateFirstName(10300, "mahesh");
		assertFalse(result);
	}

	/**
	 * This test case is used to test userService updateLastName method for positive
	 * case.
	 */
	@Test
	@Override
	public void updateLastNameTestPositive() {
		boolean result = user.updateLastName(10359, "babu");
		assertTrue(result);
	}

	/**
	 * This test case is used to test userService updateLastName method for negative
	 * case.
	 */
	@Test
	@Override
	public void updateLastNameTestNegative() {
		boolean result = user.updateLastName(10300, "babu");
		assertFalse(result);
	}

	/**
	 * This test case is used to test userService updatePhoneNumber method for
	 * positive case.
	 */
	@Test
	@Override
	public void updatePhoneNumberTestPositive() {
		boolean result = user.updatePhoneNumber(10359, "8374773600");
		assertTrue(result);
	}

	/**
	 * This test case is used to test userService updatePhoneNumber method for
	 * negative case.
	 */
	@Test
	@Override
	public void updatePhoneNumberTestNegative() {
		boolean result = user.updatePhoneNumber(10300, "8374773600");
		assertFalse(result);
	}

	/**
	 * This test case is used to test userService updateMailId method for positive
	 * case.
	 */
	@Test
	@Override
	public void updateMailIdTestPositive() {
		boolean result = user.updateMailId(10359, "mahesh@gmail.com");
		assertTrue(result);
	}

	/**
	 * This test case is used to test userService updateMailId method for negative
	 * case.
	 */
	@Test
	@Override
	public void updateMailIdTestNegative() {
		boolean result = user.updateMailId(10300, "mahesh@gmail.com");
		assertFalse(result);
	}

	/**
	 * This test case is used to test userService updateDateOfBirth method for
	 * positive case.
	 */
	@Test
	@Override
	public void updateDateOfBirthTestPositive() {
		boolean result = false;
		try {
			result = user.updateDateOfBirth(10359, new SimpleDateFormat("dd/mm/yyyy").parse("10/10/1996"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		assertTrue(result);
	}

	/**
	 * This test case is used to test userService updateDateOfBirth method for
	 * negative case.
	 */
	@Test
	@Override
	public void updateDateOfBirthTestNegative() {
		boolean result = false;
		try {
			result = user.updateDateOfBirth(10300, new SimpleDateFormat("dd/mm/yyyy").parse("10/10/1996"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		assertFalse(result);
	}

	/**
	 * This test case is used to test userService updateGender method for positive
	 * case.
	 */
	@Test
	@Override
	public void updateGenderTestPositive() {
		boolean result = user.updateGender(10359, "m");
		assertTrue(result);
	}

	/**
	 * This test case is used to test userService updateGender method for negative
	 * case.
	 */
	@Test
	@Override
	public void updateGenderTestNegative() {
		boolean result = user.updateGender(10300, "m");
		assertFalse(result);
	}

	/**
	 * This test case is used to test userService getAccountdetails method for
	 * positive case.
	 */
	@Test
	@Override
	public void getAccountdetailsTestPositive() {
		AccountBean account = user.getAccountdetails(10347);
		assertNotNull(account);
	}

	/**
	 * This test case is used to test userService getAccountdetails method for
	 * negative case.
	 */
	@Test
	@Override
	public void getAccountdetailsTestNegative() {
		AccountBean account = user.getAccountdetails(10300);
		assertNull(account);
	}

}
