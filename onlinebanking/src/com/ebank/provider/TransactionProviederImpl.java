package com.ebank.provider;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebank.beans.UserTransactionBean;
import com.ebank.service.TransactionServiceImpl;

@Path("/transaction")
public class TransactionProviederImpl implements TransactionProvider {
	/**
	 * This web resource is used to enquire balance of specific account holder.
	 */
	@GET
	@Path("/dobalenq")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public double doBalEnq(@QueryParam("accountnumber") int accountnumber) {
		return new TransactionServiceImpl().doBalEnq(accountnumber);
	}

	/**
	 * This web resource is used to deposit amount into specific account.
	 */
	@POST
	@Path("/dodeposit")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean doDeposit(@QueryParam("amount") double amount, @QueryParam("accnum") int accnum) {
		UserTransactionBean txbean = new UserTransactionBean();
		txbean.setMessage("depositing amount");
		return new TransactionServiceImpl().doDeposit(amount, accnum, txbean);
	}

	/**
	 * This web resource is used to withdraw amount from specific account.
	 */
	@POST
	@Path("/dowithdraw")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean doWithdraw(@QueryParam("amount") double amount, @QueryParam("accnum") int accnum) {
		UserTransactionBean txbean = new UserTransactionBean();
		txbean.setMessage("withdrawing amount");
		return new TransactionServiceImpl().doWithdraw(amount, accnum, txbean);
	}

	/**
	 * This web resource is used to transfer amount from specific account to another
	 * account.
	 */
	@POST
	@Path("/dotransferamount")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean doTransferAmount(@QueryParam("fromaccountnumber") int fromaccountnumber,
			@QueryParam("toaccountnum") int toaccountnumber, @QueryParam("amount") double amount) {
		UserTransactionBean txbean = new UserTransactionBean();
		txbean.setMessage("transfering amount from " + fromaccountnumber + " to " + toaccountnumber);
		return new TransactionServiceImpl().doTransferAmount(fromaccountnumber, toaccountnumber, amount, txbean);
	}

	/**
	 * This web resource is used to do current bill payment from specific account.
	 */
	@POST
	@Path("/docurrentbillpayment")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean doCurrentBillPayment(@QueryParam("amount") double amount,
			@QueryParam("accountnumber") int accountnumber) {
		UserTransactionBean txbean = new UserTransactionBean();
		txbean.setMessage("withdrawing amount");
		return new TransactionServiceImpl().doCurrentBillPayment(amount, accountnumber);
	}

	/**
	 * This web resource is used to do current bill payment from specific account.
	 */
	@POST
	@Path("/dophonebillpayment")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean doPhoneBillPayment(@QueryParam("amount") double amount,
			@QueryParam("accountnumber") int accountnumber) {
		return new TransactionServiceImpl().doPhoneBillPayment(amount, accountnumber);
	}

	/**
	 * This web resource is used to get mini-statement of specific account.
	 */
	@GET
	@Path("/ministatement")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public List<UserTransactionBean> miniStatement(@QueryParam("accountnumber") int accountnumber) {

		return new TransactionServiceImpl().miniStatement(accountnumber);
	}

	/**
	 * This web resource is used to get transaction history of specific account.
	 */
	@GET
	@Path("/transactionhistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public List<UserTransactionBean> transactionHistory(@QueryParam("accountnumber") int accountnumber) {
		List<UserTransactionBean> txlist = new TransactionServiceImpl().transactionHistory(accountnumber);
		return txlist;
	}

}
