package com.ebank.provider;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.ObjectMapper;

import com.ebank.beans.UserBean;
import com.ebank.service.UserServiceImpl;

@Path("/user")
public class UserProviderImpl implements UserProvider {
	/**
	 * This method is will execute when request for user login is came. it calls
	 * service method to authenticate. And returns response.
	 * @param userid
	 * @param password
	 * @return json string
	 */
	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean userLogin(@QueryParam("userid") int userid, @QueryParam("password") String password) {
		
		return new UserServiceImpl().doLogin(userid, password);
	} // End of userLogin

	/**
	 * This method used to handle the request of user for registration. This method
	 * calls service method to register user.
	 * 
	 * @param json
	 * @return
	 */
	@POST
	@Path("/registration")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean userRegistration(String json) {
		boolean isRegistered = false;
		UserBean user = null;
		try {
			user = new ObjectMapper().readValue(json, UserBean.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new UserServiceImpl().doRegistration(user, user.getAccount());
	} // End of userRegistration
}
