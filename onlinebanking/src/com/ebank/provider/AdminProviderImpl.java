package com.ebank.provider;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.codehaus.jackson.map.ObjectMapper;

import com.ebank.beans.AdminBean;
import com.ebank.beans.UserBean;
import com.ebank.service.AdminServiceImpl;
import com.ebank.service.UserServiceImpl;

@Path("/admin")
public class AdminProviderImpl implements AdminProvider {
	/**
	 * This method is will execute when request for admin login is came. it calls
	 * service method to authenticate. And returns response.
	 * 
	 * @param adminid
	 * @param password
	 * @return
	 */
	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean adminLogin(@QueryParam("adminid") int adminid, @QueryParam("password") String password) {
		boolean isAuthenticated = new AdminServiceImpl().doLogin(adminid, password);

		return isAuthenticated;
	} // End of adminLogin

	/**
	 * This method used to handle the request of admin for registration. This method
	 * calls service method to register admin.
	 * 
	 * @param json
	 * @return
	 */
	@POST
	@Path("/registration")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean adminRegistration(String json) {
		boolean isRegistered = false;
		AdminBean admin = null;
		try {
			admin = new ObjectMapper().readValue(json, AdminBean.class);

			isRegistered = new AdminServiceImpl().doRegistration(admin);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return isRegistered;
	} // End of adminRegistration

	/**
	 * This method used to handle the request of admin to display.This method calls
	 * service to display all user details
	 * 
	 * @param json
	 * @return
	 */

	@GET
	@Path("/displayusers")
	@Produces(MediaType.APPLICATION_JSON)
	public List<UserBean> fetchAllUsers() {
		return new UserServiceImpl().fetchAllUsers();
	}

	/**
	 * This method used to get the admin details by adminid
	 * 
	 * @param adminid
	 * @return json
	 */

	@GET
	@Path("/getadminbyid")
	@Produces(MediaType.APPLICATION_JSON)
	public AdminBean getadminbyid(@QueryParam("adminid") int adminid) {
		return new AdminServiceImpl().getAdminById(adminid);
	}

	/**
	 * This method updates already existing admin details
	 * 
	 * @param adminId
	 * @param name
	 */

	@GET
	@Path("/updatename")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean updateName(@QueryParam("adminid") int adminid, @QueryParam("name") String name) {
		return new AdminServiceImpl().updateName(adminid, name);
	}

	/**
	 * This method updates already existing admin details
	 * 
	 * @param adminId
	 * @param password
	 * @return true if updation is success else false
	 */

	@GET
	@Path("/updatepassword")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean updatePassword(@QueryParam("adminid") int adminid, @QueryParam("password") String password) {
		return new AdminServiceImpl().updatePassword(adminid, password);
	}

	/**
	 * This method updates already existing admin details
	 * 
	 * @param adminId
	 * @param gender
	 * @return true if adminid exists in the database
	 */
	@GET
	@Path("/updategender")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean updateGender(@QueryParam("adminid") int adminid, @QueryParam("gender") String gender) {
		return new AdminServiceImpl().updateGender(adminid, gender);

	}

	/**
	 * This method updates already existing admin details
	 * 
	 * @param adminId
	 * @param dateOfBirth
	 * @return true if adminid exists in the database
	 */
	@GET
	@Path("/updatedate")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean updateDate(@QueryParam("adminid") int adminid, @QueryParam("dateofbirth") Date dateofbirth) {
		return new AdminServiceImpl().updateDate(adminid, dateofbirth);
	}

	/**
	 * This method modify already existing admin details
	 * 
	 * @param adminId
	 * @param mailId
	 * @return true if adminid exists in the database
	 */
	@GET
	@Path("/updateemailid")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean updateMailId(@QueryParam("adminid") int adminid, @QueryParam("mailid") String mailid) {
		return new AdminServiceImpl().updateMailId(adminid, mailid);
	}

	/**
	 * This method updates already existing admin details
	 * 
	 * @param adminId
	 * @param phoneNum
	 * @return true if adminid exists in the database
	 */
	@GET
	@Path("/updatephonenumber")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean updatePhoneNumber(@QueryParam("adminid") int adminid,
			@QueryParam("phonenumber") String phonenumber) {
		return new AdminServiceImpl().updatePhoneNumber(adminid, phonenumber);
	}

}
