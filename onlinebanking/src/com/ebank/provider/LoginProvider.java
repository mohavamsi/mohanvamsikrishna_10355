package com.ebank.provider;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import com.ebank.service.AdminServiceImpl;
import com.ebank.service.UserServiceImpl;
import com.sun.jersey.api.view.Viewable;

/**
 * This is the resource class which acts as web resource which is specified with
 * a URL
 * @author batchE
 *
 */

@Path("/login")
public class LoginProvider {

	/**
	 * This is resource method which performs user login operation.
	 * @param userId 
	 * @param password
	 * @return it returns the result
	 */

	@Path("/user")
	@POST
	public Viewable userLogin(@FormParam("userId") int userid,@FormParam("password") String password) {
		Viewable view = null;
		boolean isExisted = new UserServiceImpl().doLogin(userid,password);
		if(isExisted) {
			view = new Viewable("/useroperations.html");
		}
		else {
			view = new Viewable("/error.html");
		}
		return view;
	}//end of user

	/**
	 * This is resource method which performs admin login operation
	 * 
	 * @param adminId
	 * @param password
	 * @return it returns the result
	 */
	@Path("/admin")
	@POST
	@Produces("text/plain")
	public Viewable AdminLogin(@FormParam("adminId") int adminId, @FormParam("password") String password) {
		Viewable view = null;
		boolean isExisted = new AdminServiceImpl().doLogin(adminId, password);
		if(isExisted) {
			view = new Viewable("/adminoperations.html");
		}else {
			view = new Viewable("/error.html");
		}
		return view;
	}//end of admin

}//end of class
