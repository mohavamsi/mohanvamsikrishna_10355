package com.ebank.provider;

import java.util.List;

import com.ebank.beans.UserTransactionBean;

public interface TransactionProvider {
	double doBalEnq(int accountnum);

	boolean doDeposit(double amount, int accNum);

	boolean doWithdraw(double amount, int accNum);

	boolean doTransferAmount(int fromAccountNum, int toAccountNum, double amount);

	boolean doCurrentBillPayment(double Amount, int AccountNumber);

	boolean doPhoneBillPayment(double Amount, int AccountNumber);

	List<UserTransactionBean> miniStatement(int AccountNumber);

	List<UserTransactionBean> transactionHistory(int accountNumber);
}
