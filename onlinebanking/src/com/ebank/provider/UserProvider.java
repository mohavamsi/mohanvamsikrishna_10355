package com.ebank.provider;

import javax.ws.rs.QueryParam;

public interface UserProvider {
	/**
	 * This method is declaration of userLogin operation
	 * 
	 * @param userid
	 * @param password
	 * @return
	 */
	boolean userLogin(@QueryParam("userid") int userid, @QueryParam("password") String password);

	/**
	 * This method is declaration of userRegistration functionality
	 * 
	 * @param json
	 * @return
	 */
	boolean userRegistration(String json);
}
