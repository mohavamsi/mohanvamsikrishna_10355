package com.ebank.provider;

import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebank.beans.AccountBean;
import com.ebank.service.AdminServiceImpl;
import com.ebank.service.BlockServiceImpl;

@Path("/blocking")
public class BlockingProviderImpl implements BlockingProvider {
	/**
	 * This method is used to check whether the user is blocked or not.
	 * @param accountNumber
	 * @return result
	 */
	@POST
	@Path("/blocked")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean isBlocked(@QueryParam("accountnumber")int accountnumber) {
		boolean result = new BlockServiceImpl().isBlocked(accountnumber);
		 return result;
	}
	 /**
	  * This method is used to Block the user.
	  * @param accountnumber
	  * @return result
	  */
	@POST
	@Path("/blockuser")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public boolean blockUser(@QueryParam("accountnumber")int accountnumber) {
		boolean result = new BlockServiceImpl().blockUser(accountnumber);
		return result;
	}
	 /**
	  * This method is used to Unblock the user.
	  * @param accountnumber
	  * @return result
	  */
	@POST
	@Path("/unblockuser")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean unBlockUser(@QueryParam("accountnumber")int accountnumber) {
		boolean result = new BlockServiceImpl().unBlockUser(accountnumber);
		return result;
	}
	/**
	 * This method is used to display all blocked Users
	 */
	@POST
	@Path("/blockallusers")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AccountBean> getAllBlockedUserList() {
		List<AccountBean> users = new BlockingProviderImpl().getAllBlockedUserList();
		return users;
	}

}
