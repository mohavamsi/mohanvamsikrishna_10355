package com.ebank.beans;

import java.util.Date;

/**
 * @author Team-E This class is an Utility class representing Admin objects And
 *         implements Serializable interface
 *
 */
public class AdminBean {


	private int adminId;
	private String adminName;
	private String password;
	private String gender;
	private Date dateOfBirth;
	private String mailId;
	private String phoneNum;

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}


	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "AdminBean [adminId=" + adminId + ", adminName=" + adminName + ", password=" + password + ", gender="
				+ gender + ", dateOfBirth=" + dateOfBirth + ", mailId=" + mailId + ", phoneNum=" + phoneNum + "]";
	}

	
}
