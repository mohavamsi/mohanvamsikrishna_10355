package com.ebank.utility;

/**
 * This interface contains method declarations which are used to read different
 * type of input from console.
 * 
 * @author Team-E
 *
 */
public interface UserInputReader {

	/**
	 * This method reads byte type input from console.
	 * 
	 * @return byte value
	 */
	byte readByte();

	/**
	 * This method reads short type input from console.
	 * 
	 * @return short value
	 */
	short readShort();

	/**
	 * This method reads int type input from console.
	 * 
	 * @return int value else 0
	 */
	int readInt();

	/**
	 * This method reads long type input from console.
	 * 
	 * @return long value
	 */
	long readLong();

	/**
	 * This method reads float type input from console.
	 * 
	 * @return float value
	 */
	float readFloat();

	/**
	 * This method reads double type input from console.
	 * 
	 * @return double value
	 */
	double readDouble();

	/**
	 * This method reads char type input from console.
	 * 
	 * @return char value
	 */
	char readChar();

	/**
	 * This method reads string type input from console. This method reads string up
	 * to the first space occurrence only.
	 * 
	 * @return String else null
	 */
	String readString();

	/**
	 * This method reads string type input from console. This method reads string
	 * value even spaces came in string. if user enter nothing empty string will
	 * return(i.e. "")
	 * 
	 * @return
	 */
	String readStringLine();
}
