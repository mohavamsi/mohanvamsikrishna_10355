package com.sample.dao;

import java.util.List;

import com.sample.bean.Student;

/**
 * This is the interface in which all the abstract classes are defined such as
 * insertion, searching, displaying, deletion and updation
 * 
 * @author IMVIZAG
 *
 */

public interface StudentDAO {

	boolean createStudent(Student student);

	Student searchById(int id);

	List<Student> getAllStudents();

	boolean deleteStudent(int id);

	boolean updateStudent(Student student);
}
