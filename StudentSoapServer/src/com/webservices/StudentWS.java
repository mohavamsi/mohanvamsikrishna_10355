package com.webservices;

import java.util.List;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

public class StudentWS {
    public Student[] getAllStudents() {
    	StudentService service = new StudentServiceImpl();
		List<Student> studentList = service.fetchAllStudents();
		Student[] students = studentList.toArray(new Student[studentList.size()]);
	    return students;
    }
    
   
    public Student findById(int id) {
    	StudentService service = new StudentServiceImpl();
		Student student = service.findById(id);
	    return student;
    }
    
    public boolean insertStudent(Student student) {
    	StudentService service = new StudentServiceImpl();
		return service.insertStudent(student);
    }
    
    
}
