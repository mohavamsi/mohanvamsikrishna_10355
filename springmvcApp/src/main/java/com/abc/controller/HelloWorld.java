package com.abc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloWorld {
	@GetMapping("/welcome")
	public String helloWorld() {
		return "sample";
	}
}
