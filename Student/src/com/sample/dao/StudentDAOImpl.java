package com.sample.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.util.DBUtil;

public class StudentDAOImpl implements StudentDAO {

	/**
	 * This methos is used to create student and insert into database
	 */
	public boolean createStudent(Student student) {
		
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		//sql query to insert data
		String sql = "insert into student_tbl values( ?, ?, ?,?,?,?,?)";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, student.getId());
			ps.setString(2, student.getName());
			ps.setInt(3, student.getAge());
			ps.setString(4, student.getPassword());
			ps.setString(5, student.getAddress().getCity());
			ps.setString(6, student.getAddress().getState());
			ps.setInt(7, student.getAddress().getPincode());
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
			
			
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	/**
	 * This is used to search the student based on Id and return true if student exists
	 */
	public Student searchById(int id) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Student st = null;
		String sql = "select * from student_tbl where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while(rs.next()) {
				Address addr=new Address();
				addr.setCity(rs.getString(5));
				addr.setState(rs.getString(6));
				addr.setPincode(rs.getInt(7));
				st= new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				st.setPassword(rs.getString(4));
				st.setAddress(addr);
				
			}
 		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return st;
	}


/**
 * this methos is used to delete the student from database with respect to id
 */
	public boolean searchByIdToDelete(int userId) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Student st = null;
		boolean result = false;
		String sql = "delete from student_tbl where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, userId);
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}


	/**
	 * 
	 * 
	 * Displays all the students present in the table
	 */
	public List<Student> getAllStudents() {
		
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Student st = null;
		List <Student> studentList = new ArrayList<Student>();
		//this query is used to display all the students
		String sql = "select * from student_tbl ";
		try {
			con = DBUtil.getCon();
			stmt= con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				st= new Student();
				Address address = new Address();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				st.setPassword(rs.getString(4));
				address.setCity(rs.getString(5));
				address.setState(rs.getString(6));
				address.setPincode(rs.getInt(7));
				st.setAddress(address);
				studentList.add(st);
			}
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return studentList;
	}

	/**
	 * This methos is used to update student records into table.
	 */
	public boolean updateStudent(Student student) {
		Connection con = null;
		PreparedStatement ps = null;
		
		String city=student.getAddress().getCity();
		String state=student.getAddress().getState();
		int pincode=student.getAddress().getPincode();
		
		int id = student.getId();
		String name =student.getName();
		int age = student.getAge();
		String password=student.getPassword();
		
		boolean result = false;
		String sql = "update student_tbl SET name = ?, age = ?, password=?, city =?,state=?,pincode=? where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setInt(2, age);
			ps.setString(3, password);
			
			ps.setString(4, city);
			ps.setString(5, state);
			ps.setInt(6, pincode);
			ps.setInt(7, id);
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected > 0) {
				result = true;
			} 
			
			
		}
		catch (SQLException e){
			System.out.println(e);
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}


}











