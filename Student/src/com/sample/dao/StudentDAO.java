package com.sample.dao;

import java.util.List;

import com.sample.bean.Student;

public interface StudentDAO {
	
	boolean createStudent( Student student );
	
	Student searchById(int id);
	
	List<Student> getAllStudents();

	boolean searchByIdToDelete(int userId);

	boolean updateStudent( Student student );
	
	
}
