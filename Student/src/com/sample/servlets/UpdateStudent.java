package com.sample.servlets;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class updateStudent
 */
@WebServlet("/updateStudent")
public class UpdateStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
	 * This method is used to send response to client in the form html page by
	 * taking the request from the client in html format
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		int studentId = Integer.parseInt(request.getParameter("id"));
		String studentName = request.getParameter("name");
		int studentAge = Integer.parseInt(request.getParameter("age"));
		String password=request.getParameter("password");
		String city=request.getParameter("city");
		String state=request.getParameter("state");
		int pincode=Integer.parseInt(request.getParameter("pincode"));
		
		//creating reference variable for Address bean
		Address address=new Address();
		address.setCity(city);
		address.setState(state);
		address.setPincode(pincode);
		
		//creating the reference object for studentbean
		Student student = new Student();
		student.setId(studentId);
		student.setName(studentName);
        student.setAge(studentAge);
		student.setPassword(password);
		student.setAddress(address);
		
		StudentService service = new StudentServiceImpl();
		//by using the reference object we can call update method in service class
		
		boolean result = service.updateStudent(student);
		RequestDispatcher rd=request.getRequestDispatcher("Update.jsp");		
		
		//if record is updated then this if block will get executed.
		if(result) {
			
			rd.forward(request, response);
		}
		else {
			response.sendRedirect("errorupdate.html");
		}
		
		
		
		
		
	}

}
