package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class Deletion
 */
@WebServlet("/Deletion")
public class DeletionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeletionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int userId = Integer.parseInt(request.getParameter("id"));
		response.setContentType("text/html");
		Student student = new Student();
		student.setId(userId);
		StudentService studentService = new StudentServiceImpl();
		boolean result= studentService.findByIdToDelete(userId);
		RequestDispatcher rd=request.getRequestDispatcher("delete.jsp");
		if (result) {
			request.setAttribute("userId", userId);
			rd.forward(request, response);
		} else {
			response.sendRedirect("deletionError.html");
		}
		
	}

}
