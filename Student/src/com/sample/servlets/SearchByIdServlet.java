package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class SearchByIdServlet
 */
@WebServlet("/SearchByIdServlet")
public class SearchByIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int userId= Integer.parseInt(request.getParameter("userId"));
		Student student=new Student();
		StudentService service=new StudentServiceImpl();
		student=service.findById(userId);
		RequestDispatcher rd=request.getRequestDispatcher("search.jsp");
		PrintWriter out = response.getWriter();
		out.println(student);
		if(student !=null) {
			request.setAttribute("student", student);
			rd.forward(request, response);
		}
		else {
	
			response.sendRedirect("SearchError.html");
		}
	}

	

}
