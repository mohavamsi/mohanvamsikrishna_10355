package com.sample.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class StudentRegistrationServlet
 */
@WebServlet("/StudentRegistrationServlet")
public class StudentRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method is used to send response to client in the form html page by
	 * taking the request from the client in html format
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// it returns the values entered from client
		int studentId = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		int age = Integer.parseInt(request.getParameter("age"));
		String password = request.getParameter("password");
		String city = request.getParameter("city");
		String state = request.getParameter("state");
	int pincode = Integer.parseInt(request.getParameter("pincode"));

		// creating reference object to Address bean
		Address addr = new Address();
		addr.setCity(city);
		addr.setState(state);
		addr.setPincode(pincode);

		// creating the reference object to Student bean
		Student student = new Student();
		student.setId(studentId);
		student.setName(name);
		student.setAge(age);
		student.setPassword(password);
		student.setAddress(addr);

		StudentService studentService = new StudentServiceImpl();

		Student result1 = studentService.findById(studentId);
		if (result1 == null) {
			// if result is true then we can insert record
			boolean result = studentService.insertStudent(student);
			RequestDispatcher rd = request.getRequestDispatcher("Registartion.jsp");
			if (result) {
				rd.forward(request, response);
			} else {
				response.sendRedirect("error.html");
			}
		} else {
			response.sendRedirect("errorRegistartion.html");
		}
	}

}
