package com.sample.service;

import java.util.List;

import com.sample.bean.Student;
import com.sample.dao.StudentDAO;
import com.sample.dao.StudentDAOImpl;


public class StudentServiceImpl implements StudentService{

	/**
	 * This method is used to insert student into table nd returns true if record is inserted
	 */
	public boolean insertStudent(Student student) {
		// TODO Auto-generated method stub
		
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.createStudent(student);
		
		return result;
	}

	/**
	 * This method is used to find the student from table based on id nd returns true if record exists
	 */
	public Student findById(int id) {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		Student student = studentDAO.searchById(id);
		return student;
	}

	/**
	 * This method is used to display all the students from table 
	 */
	public List<Student> fetchAllStudents() {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		List<Student> studentList = studentDAO.getAllStudents();
		return studentList;
	}

	/**
	 * This method is used to delete student from table and returns true if record is deleted
	 */
	public boolean findByIdToDelete(int userId) {
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.searchByIdToDelete(userId);
		return result;
	}



	/** 
	 * this method is used to update student record in the table and returns true if record is updated
	 */
	public boolean updateStudent(Student student) {
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.updateStudent(student);
		
		return result;
	}

}














