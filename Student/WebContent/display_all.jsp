<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student Details</title>
</head>
<body>
<c:forEach var ="student" items="${requestScope.studentList}">
${student.id }<br>
${student.name }<br>
${student.age }<br>
${student.address.city }<br>
${student.address.state }<br>
${student.address.pincode }<br>
<hr>
</c:forEach>
</body>
</html>