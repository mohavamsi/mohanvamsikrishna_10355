<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import = "java.util.List" %>
    <%@ page import = "com.sample.bean.Student" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student Details</title>
</head>
<body> 
	<table border='1'>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Age</th>
			<th>Password</th>
			<th>Address</th>
		</tr>
		<%
			List<Student> list = (List<Student>) request.getAttribute("studentList");
			for (Student student : list) {
		%>

		<tr>
			<td><%=student.getId() %></td>
			<td><%=student.getName()%></td>
			<td><%=student.getAge()%></td>
			<td><%=student.getPassword() %></td>
			<td><%=student.getAddress() %></td>
		</tr>
		<%
			}
		%>
	</table>
	<br>
	<a href="Home.html">Go to Home</a>
</table>

</body>
</html>








