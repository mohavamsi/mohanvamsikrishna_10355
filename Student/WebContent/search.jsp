<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>

<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<b>UserID Exists</b><br><br>
<b>ID: ${student.id}</b><br>
<b>Name: ${student.name }</b><br>
<b>Age: ${student.age }</b><br>
<b>City: ${student.address.city }</b><br>
<b>State: ${student.address.state }</b><br>
<b>Pincode: ${student.address.pincode }</b>
</body>
</html>