package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.bean.Store;
import com.abc.dao.StoreDao;

@Service
public class StoreService {
	@Autowired
	private StoreDao sdao;

	public void setSdao(StoreDao sdao) {
		this.sdao = sdao;
	}

	public Store display() {
		return sdao.display();
	}
}
