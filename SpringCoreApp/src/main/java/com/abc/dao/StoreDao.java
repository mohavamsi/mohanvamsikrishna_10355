package com.abc.dao;

import org.springframework.stereotype.Repository;

import com.abc.bean.Store;

@Repository
public class StoreDao {
	public Store display(){
		Store store = new Store();
		store.setStoreId(566748);
		store.setStoreName("Lenskart");
		return store;
	}
}
