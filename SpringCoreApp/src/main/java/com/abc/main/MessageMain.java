package com.abc.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abc.bean.Message;

public class MessageMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext context = 
				new ClassPathXmlApplicationContext("classpath:com/abc/config/context.xml");
	    Message message = (Message) context.getBean("msg");	
		message.displayMessage();
		
	}

}
