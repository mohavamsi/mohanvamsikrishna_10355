package com.abc.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abc.bean.Items;
import com.abc.service.StoreService;


public class StoreMain {
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext context = 
				new ClassPathXmlApplicationContext("classpath:com/abc/config/context.xml");
		StoreService store=(StoreService) context.getBean(StoreService.class);
	   
	   System.out.println(store.display());
//	   System.out.println(store.getStoreId());
//	   Items storeItems = store.getItem();
//	   System.out.println(storeItems.getItemId()+" "+storeItems.getItemName());
	   
	}

}

