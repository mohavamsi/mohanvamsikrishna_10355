package com.abc.bean;
/**
 * This class has item id ,name initialized.
 * @author IMVIZAG
 *
 */
public class Items {
	
	private int itemId;
	private String itemName;
	//setters and getters are created.
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	@Override
	public String toString() {
		return "Items [itemId=" + itemId + ", itemName=" + itemName + "]";
	}
	
}
