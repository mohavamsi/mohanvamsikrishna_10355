package com.abc.bean;



/**
 * This class has store details and Items class object created. 
 * @author IMVIZAG
 *
 */
public class Store {
	private int storeId;
	private String storeName;
	private Items item;//Dependency Injection
	//setter and getters created.
	public int getStoreId() {
		return storeId;
	}
	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public Items getItem() {
		return item;
	}
	public void setItem(Items item) {
		this.item = item;
	}
	@Override
	public String toString() {
		return "Store [storeId=" + storeId + ", storeName=" + storeName + ", item=" + item + "]";
	}

}//end of the class
