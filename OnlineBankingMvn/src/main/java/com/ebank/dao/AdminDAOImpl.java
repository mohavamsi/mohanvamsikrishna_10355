package com.ebank.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ebank.beans.AdminBean;
import com.ebank.util.HibernateUtil;

/**
 * This class provides implementation for AdminDAO interface
 * 
 * @author Team-E
 *
 */
public class AdminDAOImpl implements AdminDAO {

	public boolean insertAdminRecord(AdminBean admin) {
		boolean isInserted = false;
		// creating session object
		SessionFactory factory = HibernateUtil.getSessionFactory();
		try (Session session = factory.openSession()) {
			// creating transaction object
			Transaction tx = session.beginTransaction();
			// performing save operation
			session.save(admin);
			// commit transaction
			tx.commit();
			isInserted = true;
		} catch (Exception e) {

		}
		// Session session = factory.openSession();

		return isInserted;
	} // End of insertAdminRecord

	public boolean updateAdminRecord(AdminBean admin) {
		boolean isUpdated = false;
		// creating session object
		SessionFactory factory = HibernateUtil.getSessionFactory();
		try (Session session = factory.openSession()) {
			// creating transaction object
			Transaction tx = session.beginTransaction();
			// performing save operation
			session.update(admin);
			// commit transaction
			tx.commit();
			isUpdated = true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return isUpdated;
	}

	public AdminBean fetchAdminRecordById(int adminid) {
		AdminBean admin = null;
		// creating session object
		SessionFactory factory = HibernateUtil.getSessionFactory();
		try (Session session = factory.openSession()) {
			admin = session.load(AdminBean.class, adminid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return admin;
	} // End of fetchAdminRecordById

} // End of AdminDAOImpl
