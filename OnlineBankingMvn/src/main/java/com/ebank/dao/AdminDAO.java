package com.ebank.dao;

import com.ebank.beans.AdminBean;

/**
 * This interface contains method declarations which deals with admin related
 * database table
 * 
 * @author Team-E
 *
 */
public interface AdminDAO {

	boolean insertAdminRecord(AdminBean admin);

	boolean updateAdminRecord(AdminBean admin);

	AdminBean fetchAdminRecordById(int adminid);
}
