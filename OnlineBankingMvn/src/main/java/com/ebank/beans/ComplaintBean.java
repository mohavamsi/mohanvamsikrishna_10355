package com.ebank.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Team-E This class is an utility class representing User Complaints
 *         Objects which implements Serialazation
 *
 */
@Entity
@Table(name="complaints")
public class ComplaintBean {
	@Column(name="userId")
	private int userId;
	@Column(name="complaintId")
	private int complaintId;
	@Column(name="description")
	private String description;
	@Column(name="dateOfComplaint")
	private Date dateOfComplaint;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getComplaintId() {
		return complaintId;
	}

	public void setComplaintId(int complaintId) {
		this.complaintId = complaintId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateOfComplaint() {
		return dateOfComplaint;
	}

	public void setDateOfComplaint(Date dateOfComplaint) {
		this.dateOfComplaint = dateOfComplaint;
	}
}
