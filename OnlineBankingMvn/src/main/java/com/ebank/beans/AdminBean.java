package com.ebank.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Team-E This class is an Utility class representing Admin objects And
 *         implements Serializable interface
 *
 */
@Entity
@Table(name="admindetails")
public class AdminBean {

	@Id
	@Column(name="adminId")
	private int adminId;
	@Column(name="adminname")
	private String adminName;
	@Column(name="password")
	private String password;
	@Column(name="gender")
	private String gender;
	@Column(name="dateOfBirth")
	private Date dateOfBirth;
	@Column(name="mailId")
	private String mailId;
	@Column(name="phoneNum")
	private String phoneNum;
	
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}


	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
