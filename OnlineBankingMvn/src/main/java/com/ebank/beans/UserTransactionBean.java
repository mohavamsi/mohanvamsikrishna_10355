package com.ebank.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This is a bean class which represents a transaction object.
 * 
 * @author Team-E
 *
 */
@Entity
@Table(name="trasactiondetails")
public class UserTransactionBean {

	// fields declaration
	@Column(name="transactionId")
	private long transactionId;
	@Column(name="accountnumber")
	private int accountNumber;
	@Column(name="DateOfTransaction")
	private Date transactionDate;
	@Column(name="message")
	private String message;
	@Column(name="debitamount")
	private double debitAmount;
	@Column(name="creditamount")
	private double creditAmount;
	@Column(name="status")
	private String status;
	
	// setters and getters
	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
