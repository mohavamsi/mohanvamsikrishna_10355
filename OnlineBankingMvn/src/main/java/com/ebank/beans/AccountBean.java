package com.ebank.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Team-E This class is an utility class representing Account Objects
 *         which implements Serialazation
 *
 */
@Entity
@Table(name="acountdetails")
public class AccountBean {

	@Column(name="accountNum")
	private int accountNumber;
	@Column(name="accountName")
	private String accountHolderName;
	@Column(name="Balance")
	private double balance;
	@Column(name="NomineeName")
	private String nomineeName;
	@Column(name="TypeOfAccount")
	private String typeOfAccount;
	@Column(name="DateOfOpening")
	private Date openingDate;
	@Column(name="status")
	private String status;
	@Id
	@Column(name="userId")
    private int userId;
	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId; 
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountHolderNmae() {
		return accountHolderName;
	}

	public void setAccountHolderNmae(String accountHolderNmae) {
		this.accountHolderName = accountHolderNmae;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getNomineeName() {
		return nomineeName;
	}

	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	public String getTypeOfAccount() {
		return typeOfAccount;
	}

	public void setTypeOfAccount(String typeOfAccount) {
		this.typeOfAccount = typeOfAccount;
	}

	public Date getOpeningDate() {
		return openingDate;
	}

	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}

	public String isStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
