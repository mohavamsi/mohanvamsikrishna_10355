package com.ebank.service;
import java.util.Date;

import com.ebank.beans.AdminBean;

/**
 * This interface contains function declarations which are done by admin
 * 
 * @author Team-E
 *
 */
public interface AdminService {
	/**
	 * This method authenticates admin, if success let the admin login.
	 * @param adminId
	 * @param password
	 * @return true if login successful else false
	 */
	boolean doLogin(int adminId, String password);

	/**
	 * This method performs registration of admin
	 * 
	 * @param admin
	 * @return true if registration success else false
	 */
	boolean doRegistration(AdminBean admin);

	
	/**
	 * This method retrieves particular admin Object from table
	 * 
	 * @param adminId
	 * @return AdminBean
	 */
	AdminBean getAdminById(int adminId);
	
	/**
	 * This method updates already existing admin details
	 * @param adminId
	 * @param password
	 * @return true if updation is success else false
	 */
	public boolean updatePassword(int adminId, String password);
	
	/**
	 * this method is used to login admin
	 * @param adminId
	 * @return true if adminid exists in the database
	 */
	//public boolean doLogin(int adminId) ;

	/**
	 * This method updates already existing admin details
	 * @param adminId
	 * @param name
	 * @return true if adminid exists in the database
	 */
	boolean updateName(int adminId, String name);

	/**
	 * This method updates already existing admin details
	 * @param adminId
	 * @param gender
	 * @return true if adminid exists in the database
	 */
	boolean updateGender(int adminId, String gender);
	
	/**
	 * This method updates already existing admin details
	 * @param adminId
	 * @param dateOfBirth
	 * @return true if adminid exists in the database
	 */
	public boolean updateDate(int adminId, Date dateOfBirth);

	/**
	 * This method updates already existing admin details
	 * @param adminId
	 * @param mailId
	 * @return true if adminid exists in the database
	 */
	boolean updateMailId(int adminId, String mailId);

	/**
	 * This method updates already existing admin details
	 * @param adminId
	 * @param phoneNum
	 * @return true if adminid exists in the database
	 */
	boolean updatePhoneNumber(int adminId, String phoneNum);
}
