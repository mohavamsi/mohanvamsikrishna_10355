package com.abc.pojo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="mob")
public class AccPojo {
	
	@Id
	@Column(name="accNumber")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int accNumber;
	@Column(name="accName")
	private String accName;
	@Column(name="age")
	private int age;
	@Column(name="accId")
	private int accId;
	
	private AccDetails accDetails;
	public int getAccNumber() {
		return accNumber;
	}
	public void setAccNumber(int accNumber) {
		this.accNumber = accNumber;
	}
	public String getAccName() {
		return accName;
	}
	public void setAccName(String accName) {
		this.accName = accName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getAccId() {
		return accId;
	}
	public void setAccId(int accId) {
		this.accId = accId;
	}
	@OneToOne(cascade=CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="accId")
	public AccDetails getAccDetails() {
		return accDetails;
	}
	public void setAccDetails(AccDetails accDetails) {
		this.accDetails = accDetails;
	}
	
	
	
}
