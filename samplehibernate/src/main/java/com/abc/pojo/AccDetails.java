package com.abc.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class has all account details  attributes. 
 * @author IMVIZAG
 *
 */
@Entity
@Table(name="AccDetails")
public class AccDetails {
//account details attributes are initiliazed
	private int accId;
	private double balance;
	private String bank;
	private String ifscCode;
//setters and getters are created
	@Id
	@GeneratedValue
	@Column(name="accId_PK")
	public int getAccId() {
		return accId;
	}
	public void setAccdetailId(int accId) {
		this.accId = accId;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}
	
}
