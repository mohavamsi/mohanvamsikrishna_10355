package com.abc.main;

import java.lang.annotation.Annotation;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationException;

import com.abc.pojo.AccDetails;
import com.abc.pojo.AccPojo;
import com.abc.util.HibernateUtil;
/**
 * This class has main method to add operation into the table.
 * @author IMVIZAG
 *
 */
public class AccMain {
	//main started
public static void main(String[] args) {
	AnnotationException config = new AnnotationConfiguration();
		config.addAnnotatedClass(AccPojo.class);
		config.addAnnotatedClass(AccDetails.class);
		config.configure("hibernate.cfg.xml");
		
	SessionFactory sessionfactory = HibernateUtil.getSessionFactory();
	
	AccDetails accdetails = new AccDetails();
	accdetails.setAccdetailId(567);
	accdetails.setBalance(100000);
	accdetails.setBank("sc");
	accdetails.setIfscCode("SC007MVK");
	AccPojo accpojo = new AccPojo();
	accpojo.setAccName("mohan");
	accpojo.setAge(21);
	accpojo.setAccId(567);      
	accpojo.setAccDetails(accdetails);
	Session session = sessionfactory.openSession();
	session.save(accpojo);
//	session.save(accdetails);
	Transaction txn = session.beginTransaction();
	txn.commit();
	session.close();
	HibernateUtil.shutdown();
	System.out.println("Acount saved");
	}
}
