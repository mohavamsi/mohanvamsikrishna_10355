package com.abc.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.abc.entity.Product;
import com.abc.util.HibernateUtil;

public class ProductDAO {
	/**
	 * This method is used to insert the values into the table
	 * @param product
	 * @return
	 */
	public boolean create(Product product) {
		SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		
		session.save(product);
		System.out.println("product saved");
		txn.commit();
		session.close();
		return true;
	}
	/**
	 * This method is used to display all products from the table
	 * 
	 */
	public static  void displayall() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		List name = session.createQuery("from Product").list();
		Iterator itr = name.iterator();
		while(itr.hasNext()) {
		Product	product = (Product) itr.next();
			System.out.println(product.getProduct_ID()+product.getProduct_Name()+product.getProductPrice()+product.getProduct_Category());
		}
		txn.commit();
		session.close();
	}
	/**
	 * This method is used to delete the product from the table
	 * @param productId
	 */
	public void delete(String productId) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		
		Product product = (Product)session.get(Product.class, productId);
		session.delete(product);
		
		txn.commit();
		 session.close();
		}
	public void update(String productId) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		
		Product product = (Product)session.get(Product.class, productId);
		session.delete(product);

		txn.commit();
		 session.close();
	}
	
}
